#encoding: utf-8

# https://github.com/ruby-rdf/rdf-sesame

require 'rdf'
require 'sparql/client'
require 'rdf/sesame.rb'
require 'rest-client'
require 'active_support'

begin
#choose operation by assigning (create, delete, read, update)  
   op = "create"
#   op = "delete"
#   op = "read"
#   op = "update"
   
   
   ID = "12325"
#----------------------- create person
  if op == "create"
 #rest_service_addr      = 'http://172.17.20.77:3000/entities'
 rest_service_addr      = 'http://10.10.4.15/infoport/entities'
  
  # query_text = "DELETE {?s ?p ?v} WHERE {?s ?p ?v }"  
  #TEST DATA
data = Hash.new
data[:id] = ID
data[:lastname] = "Фамилия1"+data[:id].to_s
data[:firstname] = "Имя1" 
data[:middlename] = "Отчество1"
data[:image] ="http://www.hse.ru/data/2010/10/22/1234493283/1babkin_HSE.png"
data[:keywords] = ["К111","К222","333"] 
data[:units] = ["Кафедра математики"]
data[:topics] = ["Пропаганда и популяризация знаний"]
data[:articles] = ["My Article 1"]


data1 = Hash.new
data1[:data]= data


rawdata = <<-EOJSON
{"data":{
  "articles":["http://publications.hse.ru/view/62672285","http://www.hse.ru/en/org/persons/201585"],
  "firstname":"aa",
  "middlename":"aa",
  "lastname":"это то то.... \u041a\u0430\u0440\u043f\u043e\u0432",
  "topics":["20.53.00 \u0422\u0435\u0445\u043d\u0438\u0447\u0435\u0441\u043a\u0438\u0435"],
  "id":"67893",
  "course":["\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442"],
  "keywords":["\u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0435"],
  "units":["Кафедра математики"],"image":"hhh"}}
EOJSON



rawdata = <<-EOJSON
{"data": {"articles": ["http://publications.hse.ru/view/62672285", "http://www.hse.ru/en/org/persons/201585", "http://publications.hse.ru/view/65653823", "http://publications.hse.ru/view/60439026", "http://publications.hse.ru/view/65609010", "http://publications.hse.ru/view/62029967", "http://publications.hse.ru/view/75303516"], "firstname": "\u041d\u0438\u043a\u043e\u043b\u0430\u0439", "middlename": "\u0412\u044f\u0447\u0435\u0441\u043b\u0430\u0432\u043e\u0432\u0438\u0447", "lastname": "\u041a\u0430\u0440\u043f\u043e\u0432", "topics": ["20.53.00 \u0422\u0435\u0445\u043d\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0441\u0440\u0435\u0434\u0441\u0442\u0432\u0430 \u043e\u0431\u0435\u0441\u043f\u0435\u0447\u0435\u043d\u0438\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0445 \u043f\u0440\u043e\u0446\u0435\u0441\u0441\u043e\u0432", "20.23.00 \u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u043f\u043e\u0438\u0441\u043a", "20.00.00 \u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0442\u0438\u043a\u0430"], "id": "26810184", "keywords": ["\u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u0440\u0430\u0441\u043f\u043e\u0437\u043d\u0430\u0432\u0430\u043d\u0438\u0435 \u0440\u0435\u0447\u0438", "\u0430\u043d\u0430\u043b\u0438\u0437 \u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0430 \u0440\u0435\u0447\u0438", " \u0430\u043d\u0430\u043b\u0438\u0437 \u0434\u0430\u043d\u043d\u044b\u0445", "\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u043e\u043d\u043d\u044b\u0439 \u043f\u043e\u0438\u0441\u043a", "\u043d\u043e\u0432\u044b\u0435 \u043e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u0442\u0435\u0445\u043d\u043e\u043b\u043e\u0433\u0438\u0438", "\u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0430 \u0442\u0435\u043a\u0441\u0442\u0430"], "image": "", "units": ["\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442 \u0431\u0438\u0437\u043d\u0435\u0441-\u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0442\u0438\u043a\u0438 \u0438 \u043f\u0440\u0438\u043a\u043b\u0430\u0434\u043d\u043e\u0439 \u043c\u0430\u0442\u0435\u043c\u0430\u0442\u0438\u043a\u0438 (\u041d\u0438\u0436\u043d\u0438\u0439 \u041d\u043e\u0432\u0433\u043e\u0440\u043e\u0434)", "\u041a\u0430\u0444\u0435\u0434\u0440\u0430 \u043f\u0440\u0438\u043a\u043b\u0430\u0434\u043d\u043e\u0439 \u043c\u0430\u0442\u0435\u043c\u0430\u0442\u0438\u043a\u0438 \u0438 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0442\u0438\u043a\u0438 (\u041d\u0438\u0436\u043d\u0438\u0439 \u041d\u043e\u0432\u0433\u043e\u0440\u043e\u0434)"], "campus": "\u041d\u0438\u0436\u043d\u0438\u0439 \u041d\u043e\u0432\u0433\u043e\u0440\u043e\u0434"}}
EOJSON



rawdata= <<EOJSON1
{"{\"data\": {\"articles\": "=>{"\"http://publications.hse.ru/view/62672285\", \"http://www.hse.ru/en/org/persons/201585\", \"http://publications.hse.ru/view/65653823\", \"http://publications.hse.ru/view/60439026\", \"http://publications.hse.ru/view/65609010\", \"http://publications.hse.ru/view/62029967\", \"http://publications.hse.ru/view/75303516\""=>{", \"firstname\": \"\\u041d\\u0438\\u043a\\u043e\\u043b\\u0430\\u0439\", \"middlename\": \"\\u0412\\u044f\\u0447\\u0435\\u0441\\u043b\\u0430\\u0432\\u043e\\u0432\\u0438\\u0447\", \"lastname\": \"\\u041a\\u0430\\u0440\\u043f\\u043e\\u0432\", \"topics\": "=>{"\"20.53.00 \\u0422\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0430 \\u043e\\u0431\\u0435\\u0441\\u043f\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441\\u043e\\u0432\", \"20.23.00 \\u0418\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0439 \\u043f\\u043e\\u0438\\u0441\\u043a\", \"20.00.00 \\u0418\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u043a\\u0430\""=>{", \"id\": \"26810183\", \"keywords\": "=>{"\"\\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0435 \\u0440\\u0430\\u0441\\u043f\\u043e\\u0437\\u043d\\u0430\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0440\\u0435\\u0447\\u0438\", \"\\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u0440\\u0435\\u0447\\u0438\", \" \\u0430\\u043d\\u0430\\u043b\\u0438\\u0437 \\u0434\\u0430\\u043d\\u043d\\u044b\\u0445\", \"\\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0439 \\u043f\\u043e\\u0438\\u0441\\u043a\", \"\\u043d\\u043e\\u0432\\u044b\\u0435 \\u043e\\u0431\\u0440\\u0430\\u0437\\u043e\\u0432\\u0430\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438\", \"\\u0430\\u0432\\u0442\\u043e\\u043c\\u0430\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043e\\u0431\\u0440\\u0430\\u0431\\u043e\\u0442\\u043a\\u0430 \\u0442\\u0435\\u043a\\u0441\\u0442\\u0430\""=>{", \"image\": \"\", \"units\": "=>{"\"\\u0424\\u0430\\u043a\\u0443\\u043b\\u044c\\u0442\\u0435\\u0442 \\u0431\\u0438\\u0437\\u043d\\u0435\\u0441-\\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u043a\\u0438 \\u0438 \\u043f\\u0440\\u0438\\u043a\\u043b\\u0430\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0430\\u0442\\u0435\\u043c\\u0430\\u0442\\u0438\\u043a\\u0438 (\\u041d\\u0438\\u0436\\u043d\\u0438\\u0439 \\u041d\\u043e\\u0432\\u0433\\u043e\\u0440\\u043e\\u0434)\", \"\\u041a\\u0430\\u0444\\u0435\\u0434\\u0440\\u0430 \\u043f\\u0440\\u0438\\u043a\\u043b\\u0430\\u0434\\u043d\\u043e\\u0439 \\u043c\\u0430\\u0442\\u0435\\u043c\\u0430\\u0442\\u0438\\u043a\\u0438 \\u0438 \\u0438\\u043d\\u0444\\u043e\\u0440\\u043c\\u0430\\u0442\\u0438\\u043a\\u0438 (\\u041d\\u0438\\u0436\\u043d\\u0438\\u0439 \\u041d\\u043e\\u0432\\u0433\\u043e\\u0440\\u043e\\u0434)\""=>{", \"campus\": \"\\u041d\\u0438\\u0436\\u043d\\u0438\\u0439 \\u041d\\u043e\\u0432\\u0433\\u043e\\u0440\\u043e\\u0434\"}}"=>nil}}}}}}}}}
EOJSON1




#rawdata = <<-EOJSON
#{"data":{"firstname":"aa","middlename":"aa","lastname":"aaa","id":"67891","image":"hhh"}}
#EOJSON



wrong_input_data = "{\"data\": {\"json_data\":\"aaa\"} }"

json_data =  ActiveSupport::JSON.encode(data1)

puts json_data


  resp = RestClient.post rest_service_addr + ".json", json_data,
     {   
 #             :data       =>  rawdata,    
              :content_type => "application/json; charset=utf-8"                #"application/x-www-form-urlencoded;charset=utf-8"        
     }
    
#resp = RestClient.post rest_service_addr + ".json", json_data, :content_type => "application/json"   
    
    #resp = resp.code   

    puts resp
  end
   
   
#----------------------- delete person
  if op == "delete"
 #     rest_service_addr      = 'http://172.17.22.17:3000/entities'
      rest_service_addr      = 'http://10.10.4.15/infoport/entities'  
    data =  ID
        
    resp = RestClient.delete rest_service_addr+"/"+data.to_s,
         {   
                  :id       =>  data,    
                  :content_type => "text/html;charset=utf-8"                #"application/x-www-form-urlencoded"        
         }
        
        
        
        
       
        #resp = resp.code   
        puts resp
  end
 
 
#----------------------- read person
  if op == "read"
      rest_service_addr      = 'http://172.17.22.17:3000/entities'
      
      data =  12345
      resp = RestClient.get rest_service_addr+"/"+data.to_s,
      {   
                  :content_type => "text/html;charset=utf-8"                #"application/x-www-form-urlencoded"        
      }         
      #resp = resp.code   
      puts resp
  end 
  
#----------------------- update person
  if op == "update"
     #TEST DATA
    data = Hash.new
    data[:id] = 12345
    data[:lastname] = "bbb"+data[:id].to_s
    data[:image] ="http://image.com/3333.png"
    data[:keywords] = ["111","222","333", "444"] 
    data[:units] = ["Кафедра математики"]
    data[:topics] = ["Пропаганда и популяризация знаний"]
    
      rest_service_addr      = 'http://172.17.22.17:3000/entities'
      
     
      resp = RestClient.put rest_service_addr+"/"+ data[:id].to_s,
      {           :data       =>  data,
                  :content_type => "text/html;charset=utf-8"                #"application/x-www-form-urlencoded"        
      }     
      #resp = resp.code   
      puts resp
    
 end   
 
 rescue RestClient::Exception => e 
   puts " ======== REST CLIENT Exception during submit \n"
   
   puts "HTTP BODY ----------- begin:"
   puts e.http_body
   puts "HTTP BODY ----------- end:"
   puts "HTTP CODE:" + e.http_code.to_s
   
   puts `git describe --always`
end