#encoding: utf-8

require 'rdf'
require 'rest-client'
require 'rdf/sesame.rb'
require 'hash_validator'


#DONE add taxonomy topics (attrs and fetch)
#DONE add units (attrs and fetch)

class Person
  attr_reader :uri, :lastname, :firstname, :middlename, :img, :units, :keywords, :taxo_topics 
  attr_writer :units, :keywords, :taxo_topics
  

#VALIDATION TEMPLATES
    # Template CREATE ----------------------- begin --
        VALIDATION_CREATE = 
        {
            id:         'string',
            lastname:   'string',
            firstname:  'string',
            middlename: 'string',
            image:      'string',
            firstname:  'string',
            articles:   'array',
            keywords:   'array',
            units:      'array',
            topics:     'array'       
      }
    # Template CREATE ----------------------- end --
  
#-----------------------------------------------------------------------------------  
  def initialize (uri, lastname, firstname= nil, middlename= nil, img= nil)
    @uri= uri
    @lastname   = lastname
    @firstname  = firstname
    @middlename = middlename
    @img        = img
    
  end
#=========================================== ACCESSORS OF OPERATING REPOSITORY ===============================
#-------------------------------------------------------------------------------------------------------------  
def fetchDetails
  #@img
  
      addr = Infoport::Application.config.person_repo 
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})

      query_text = "SELECT ?im WHERE {<#{@uri}> <http://xmlns.com/foaf/0.1/img> ?im .}"
 
      solutions = client.query(query_text);
      
      if solutions.present?
       @img = solutions.first[:im]
      else
       @img = nil
      end  
  
  #@keywords
      query_text = "SELECT ?l WHERE {<#{@uri}> <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?l.}"
      solutions = client.query(query_text);
      
      @keywords = []; 
      if solutions.present?
          solutions.each do |s|
            @keywords << s[:l]
          end    
      end
           
  
  #@units 
      query_text = "SELECT ?n WHERE {<#{@uri}> <http://www.w3.org/ns/org#memberOf>  ?u.  SERVICE <#{Infoport::Application.config.org_repo}> {?u  <http://xmlns.com/foaf/0.1/name> ?n.} }"
      solutions = client.query(query_text);
      
      @units = [];
      if solutions.present?
          solutions.each do |s|
            @units << s[:n]
          end         
      end
      
      
  #@taxo_topics
      query_text = "SELECT ?l WHERE {<#{@uri}> <http://www.hse.ru/ontologies/v1/infoport#classify> ?c. SERVICE <#{Infoport::Application.config.taxonomy_repo}> {?c  <http://www.w3.org/2004/02/skos/core#prefLabel> ?l.} }"
      solutions = client.query(query_text);
     
     @taxo_topics = [];
      if solutions.present?
        solutions.each do |s|
          @taxo_topics << s[:l]
        end     
      end 
end   
#-----------------------------------------------------------------------------------  
#FIXME check also the type of RDF entity (Person)
def self.all
   addr = Infoport::Application.config.person_repo
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
      
      query_text = "SELECT * WHERE {?p <http://xmlns.com/foaf/0.1/lastName> ?ln. OPTIONAL { ?p <http://xmlns.com/foaf/0.1/firstName> ?fn } OPTIONAL {?p <http://xmlns.com/foaf/0.1/middleName> ?mn } }" 
      solutions = client.query(query_text);

      results = Array.new
      if solutions.present?
        
        solutions.each do |s|
        found_p = Person.new(s[:p], s[:ln], s[:fn], s[:mn])
        results << found_p  
        end         
      end
 
      results     
end

#-----------------------------------------------------------------------------------  
def self.find_by_uri(uri, fetch_details = true)
      addr = Infoport::Application.config.person_repo
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
      
      query_text = "SELECT * WHERE {<#{uri}> <http://xmlns.com/foaf/0.1/lastName> ?ln. OPTIONAL { <#{uri}> <http://xmlns.com/foaf/0.1/firstName> ?fn } OPTIONAL {<#{uri}> <http://xmlns.com/foaf/0.1/middleName> ?mn } }" 
      solutions = client.query(query_text);

      if solutions.present?
        found_p = Person.new(uri, solutions.first[:ln], solutions.first[:fn], solutions.first[:mn])
        found_p.fetchDetails if fetch_details         
      else
        found_p = nil    
      end
 
      found_p     
end   
   
#-----------------------------------------------------------------------------------   
  def self.find_by_conditions (current_page, org_level, keys, taxonomy_topics)
    
      keys_set = nil
      elib_set = nil
    
    if keys.present?
      keys_set = keys.to_s
      keys_set[0]='('
      keys_set[ keys_set.length-1]=')'
    end  
      
      if taxonomy_topics.present?
        elib_set = taxonomy_topics.to_s
        elib_set[0]='('
        elib_set[elib_set.length-1]=')'
        elib_set.gsub!('"', ' ')
      end
    
    #TODO investigate if RuBY yield construct is feasible here ?
    
    #org structure constraints     
    query_text  = "SELECT DISTINCT ?person ?lname ?fname ?mname WHERE { ?person <http://www.w3.org/ns/org#memberOf>  ?unit . ?person <http://xmlns.com/foaf/0.1/lastName> ?lname. SERVICE <#{Infoport::Application.config.org_repo}> {<#{org_level}> <http://www.w3.org/ns/org#hasUnit>*  ?unit .} "
    #FIXME SERVICE clause SOMETIMES does not work in Person query
    #query_text = "SELECT DISTINCT ?person ?lname ?fname ?mname WHERE { ?person <http://www.w3.org/ns/org#memberOf>  ?unit . ?person <http://xmlns.com/foaf/0.1/lastName> ?lname. <#{org_level}> <http://www.w3.org/ns/org#hasUnit>*  ?unit . "
    query_text << " OPTIONAL {?person <http://xmlns.com/foaf/0.1/firstName> ?fname } OPTIONAL { ?person <http://xmlns.com/foaf/0.1/middleName> ?mname } "
          
      if keys_set.present?
      # make for each of the keys
      query_text << "?person   <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?k. "
      end  
    
      if elib_set.present?
      # make for each e_lib_topic 
        query_text << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c.  }"
        query_text << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c. SERVICE <#{Infoport::Application.config.taxonomy_repo}> {?c <http://www.w3.org/2004/02/skos/core#broader>+ ?c1.} }"
      end
      
      if keys_set.present?
       query_text <<  " FILTER ( ?k in #{keys_set})"
      end
      
      if elib_set.present? 
       query_text << " FILTER ( ?c in #{elib_set} || (?c1 in #{elib_set} ))"
      end 
      
      offset = (current_page -1) * Infoport::Application.config.found_entities_per_page
      
      query_text << " } ORDER BY (?lname) OFFSET #{offset} LIMIT #{Infoport::Application.config.found_entities_per_page} "
    
      addr = Infoport::Application.config.person_repo 
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
    
      solutions = client.query(query_text);
    
      records = []
      if solutions.present?
            
      #[].tap do |records|
                 solutions.each do |sol| 
                            #records <<
                            # p =  Person.unserialize(sol[:person])
                            p = Person.new(sol[:person], sol[:lname], sol[:fname], sol[:mname] )
                            #p.initialize(sol[:person], sol[:lname])
                            records <<  p
                 end  
       #end
       end
       records 
       
    #TODO catch and process exceptions here   
          
   end
  
#-------------------------------------------------------------------------------------------------------------
#TODO refactor this method and find_by_conditions  in accordance with DRY principle !!!
 def self.getSummary (org_level, keys, taxonomy_topics )
      #execute SPARQL aggregate query
      #"<http://www.hse.ru/ontologies/v1/infoport#NNCampus>"
     
      keys_set = nil
      elib_set = nil
    
    if keys.present?
      keys_set = keys.to_s
      keys_set[0]='('
      keys_set[ keys_set.length-1]=')'
    end  
      
      if taxonomy_topics.present?
        elib_set = taxonomy_topics.to_s
        elib_set[0]='('
        elib_set[elib_set.length-1]=')'
        elib_set.gsub!('"', ' ')
      end
    
      query_text = "SELECT ?l (COUNT (?l) as ?lC) WHERE {  ?person <http://www.w3.org/ns/org#memberOf>  ?unit .  SERVICE <#{Infoport::Application.config.org_repo }> {<#{org_level}> <http://www.w3.org/ns/org#hasUnit>*  ?unit . } "
      query_text_2 = "SELECT (COUNT (DISTINCT ?person) AS ?c) WHERE { ?person <http://www.w3.org/ns/org#memberOf> ?unit . SERVICE <#{Infoport::Application.config.org_repo }> {<#{org_level}> <http://www.w3.org/ns/org#hasUnit>*  ?unit .} "
      
      # in any case append keys relations
      # make for each of the keys
      query_text << "?person <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?l. "
      query_text_2 << "?person <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?l. "
      
    
      
      if elib_set.present?
      # make for each e_lib_topic 
        query_text << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c.  }"
        query_text << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c. SERVICE <#{Infoport::Application.config.taxonomy_repo}> {?c <http://www.w3.org/2004/02/skos/core#broader>+ ?c1. } }"
      
        query_text_2 << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c.  }"
        query_text_2 << " OPTIONAL { ?person <http://www.hse.ru/ontologies/v1/infoport#classify> ?c. SERVICE <#{Infoport::Application.config.taxonomy_repo}> {?c <http://www.w3.org/2004/02/skos/core#broader>+ ?c1.} }"    
      end
            
      if keys_set.present?
      # new feature: add extra variable l2 to show all keywords of the persons in summary instead the filter condition only
       query_text <<  " ?person <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?l2.  FILTER ( ?l2 in #{keys_set})"
       query_text_2 <<  " FILTER ( ?l in #{keys_set})"
      end
      
      if elib_set.present? 
       query_text << " FILTER ( ?c in #{elib_set} || (?c1 in #{elib_set} ))"
       query_text_2 << " FILTER ( ?c in #{elib_set} || (?c1 in #{elib_set} ))"
      end 
      
     #FIXME unlimited number of keywords returned causes exception inside java recv
     query_text << " } GROUP BY ?l ORDER BY DESC(?lC)" #LIMIT 50"   
     query_text_2 << " }"   
        
     addr = Infoport::Application.config.person_repo 
     client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
    
    #puts "Q SUMMARY ===============>\n"
    #puts query_text
    #puts query_text_2
    
     solutions = client.query(query_text);
     solutions_2 = client.query(query_text_2);
     
     result = Hash.new   
     result["hist"] = solutions
     result["entity_count"] = solutions_2.first[:c].to_i
  
  result  
  end
    
 #-------------------------------------------------------------------------------------------------------------
 def self.getAllKeys (template )
   
     addr = Infoport::Application.config.person_repo 
     client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
     
     # http://jena.sourceforge.net/ARQ/Tutorial/filters.html
     query_text="SELECT DISTINCT ?fk ?l WHERE {?person <http://www.hse.ru/ontologies/v1/infoport#FreeKeyword> ?l. FILTER regex (?l , \"^#{template}\", \"i\")}"
     
     solutions = client.query(query_text);
     
     results = []
     
     if solutions.present?
         solutions.each do |s|
          results << {:id => s[:l].to_s, :text=>s[:l].to_s}
         end
     end
     
    results 
 end 
  
#-------------------------------------------------------------------------------------------------------------
# TODO check additonally proper rdf-class of uri 
 def self.exist?( uri)
 
 result = true
   
  addr = Infoport::Application.config.person_repo 
  client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
   
 result = client.ask.whether([RDF::URI(uri), :p, :o]).true?
 result    
 end
#--------------------------------------------------------------------------------------------------------------
def self.validateData(hash_data, validation_template)


    validator = HashValidator.validate(hash_data, validation_template)

    validator
end


#=========================================== MODIFIERS OF OPERATING REPOSITORY ===============================
#-------------------------------------------------------------------------------------------------------------
def destroy
  addr = Infoport::Application.config.person_repo 
  query_text = "DELETE { <#{@uri}> ?p ?v } WHERE { <#{@uri}> ?p ?v .}"              
            
  # USE SESAME-Specific HTTP REST Interface
  resp = RestClient.post addr +"/statements", "update="+query_text, { :content_type => "application/x-www-form-urlencoded; charset=utf-8" }

#     {   
#              :update       => query_text,    
#              :content_type => "text/html;charset=utf-8"                #"application/x-www-form-urlencoded"        
#     }    
#    resp = resp.code   
#    puts resp    
end
  


#-------------------------------------------------------------------------------------------------------------
def save
  
 addr = Infoport::Application.config.person_repo 

#save all rdf-triples to the intermediate graph
 graph = RDF::Graph.new 
    
# RDF::Transaction.execute(repository) do |tx|
          subject = RDF::URI(@uri)
          graph << [subject, RDF::type, Infoport::Application.config.rdf_baseuri + "Person"]
          graph << [subject, RDF::FOAF::lastName, @lastname]
          graph << [subject, RDF::FOAF::img, @img]  if@img.present?
          graph << [subject, RDF::FOAF::firstName, @firstname] if@firstname.present?
          graph << [subject, RDF::FOAF::middleName, @middlename] if@middlename.present?
           
    
           #iterate over keywords to insert correspondent RDF triple
           if @keywords.present?
             @keywords.each do |k|
                graph << [subject, RDF::URI("http://www.hse.ru/ontologies/v1/infoport#FreeKeyword"), k]   
             end
           end
    
         #iterate over units to find correspondent unit URI
         if @units.present?
           @units.each do |uname|
            query_text = "SELECT ?u WHERE { ?u  <http://xmlns.com/foaf/0.1/name>  \"#{uname}\"^^xsd:string .}"
            
            puts "query ----> UNIT"
            puts query_text
            
            client = SPARQL::Client.new(Infoport::Application.config.org_repo , { :method => "get", :protocol=>"1.1"})
            solutions = client.query(query_text);
                if solutions.present?
                  org_uri = solutions.first[:u] 
                  puts "ORG UNIT--->\n"
                  puts org_uri
                  graph << [subject, RDF::URI("http://www.w3.org/ns/org#memberOf"), org_uri ]
                end    
            end
         end  

       #iterate over taxonomy topics to find correspondent topic URI
       if @taxo_topics.present?
         @taxo_topics.each do |t|
          query_text = "SELECT ?u WHERE { ?u  <http://www.w3.org/2004/02/skos/core#prefLabel> \"#{t}\" .}"
          client = SPARQL::Client.new(Infoport::Application.config.taxonomy_repo , { :method => "get", :protocol=>"1.1"})
          solutions = client.query(query_text);
            if solutions.present?
              topic_uri = RDF::URI(solutions.first[:u])
              graph << [subject, RDF::URI(Infoport::Application.config.rdf_baseuri+"classify"), topic_uri ]
            end    
          end 
        end  
         
  #  output intermediate graph to the string    
  output = RDF::Writer.for(:ntriples).buffer do |writer|
       graph.each_statement do |st|
        writer << st
       end
  end
  
  #bulk load of the statements to the rdf repository
  headers = { :content_type => "application/x-turtle; charset=utf-8" } #OK
  resp = RestClient.post( addr  + "/statements", output, headers  ) # OK
  puts "RESPOND FROM BULK LOAD=" + resp.code.to_s
  
 end  


#=========================================== MODIFIERS OF HISTORICAL REPOSITORY ===============================
# all methods are private and are called from the correspondent operating methods

#-------------------------------------------------------------------------------------------------------------
def histStateStart
#TODO check presence of the instance in the historical repo. Correct pre-condition: such instance is missing, else -  ERROR !

#save all rdf-triples to the intermediate graph
 graph = RDF::Graph.new 

 addr = Infoport::Application.config.historical_repo 

 #create new State instance for the person
    bnode = RDF::URI(Infoport::Application.config.rdf_baseuri + UUID::generate)
    timestamp = RDF::Literal.new(DateTime.now)

 
 #create a graph of State instance  for the perosn  
     graph << [bnode, RDF::type, RDF::URI(Infoport::Application.config.rdf_baseuri+"State")]  
     graph << [bnode, RDF::subject, RDF::URI(@uri)]  
     graph << [bnode, RDF::predicate, RDF::type]
     graph << [bnode, RDF::object, RDF::URI(Infoport::Application.config.rdf_baseuri+"Person")]  
     graph << [bnode, RDF::URI(Infoport::Application.config.rdf_baseuri+"TimeStart"), timestamp ]  

           #iterate over person's keywords to create corresponding State instance for the keywords
           if @keywords.present?
             @keywords.each do |k|
                bnode = RDF::URI(Infoport::Application.config.rdf_baseuri + UUID::generate)
                 graph << [bnode, RDF::type, RDF::URI(Infoport::Application.config.rdf_baseuri+"State")]
                 graph << [bnode, RDF::subject, RDF::URI(@uri)]  
                 graph << [bnode, RDF::predicate, RDF::URI(Infoport::Application.config.rdf_baseuri+"FreeKeyword")]
                 graph << [bnode, RDF::object, k]
                 graph << [bnode, RDF::URI(Infoport::Application.config.rdf_baseuri+"TimeStart"), timestamp ]  
             end
           end

           #iterate over person's units to create corresponding State instance for the units
           if @units.present?
             @units.each do |uname|
                bnode = RDF::URI(Infoport::Application.config.rdf_baseuri + UUID::generate)
                 graph << [bnode, RDF::type, RDF::URI(Infoport::Application.config.rdf_baseuri+"State")]
                 graph << [bnode, RDF::subject, RDF::URI(@uri)]  
                 graph << [bnode, RDF::predicate, RDF::URI("http://www.w3.org/ns/org#memberOf")]
                 graph << [bnode, RDF::object, uname ]
                 graph << [bnode, RDF::URI(Infoport::Application.config.rdf_baseuri+"TimeStart"), timestamp ]  
               end    
            end  

           #iterate over person's taxonomy topics to create corresponding State instance for the topics
           if @taxo_topics.present?
             @taxo_topics.each do |t|
                bnode = RDF::URI(Infoport::Application.config.rdf_baseuri + UUID::generate)
                 graph <<  [bnode, RDF::type, RDF::URI(Infoport::Application.config.rdf_baseuri+"State")]
                 graph <<  [bnode, RDF::subject, RDF::URI(@uri)]  
                 graph <<  [bnode, RDF::predicate, RDF::URI(Infoport::Application.config.rdf_baseuri+"classify")]
                 graph <<  [bnode, RDF::object, t ]
                 graph <<  [bnode, RDF::URI(Infoport::Application.config.rdf_baseuri+"TimeStart"), timestamp ]  
                end    
           end  
           
#----- REST-client --- begin
 #  output intermediate graph to the string    
  output = RDF::Writer.for(:ntriples).buffer do |writer|
       graph.each_statement do |st|
        writer << st
       end
  end
  
  #bulk load of the statements to the rdf repository
  headers = { :content_type => "application/x-turtle; charset=utf-8" } #OK
  resp = RestClient.post( addr  + "/statements", output, headers  ) # OK
  puts "RESPOND FROM BULK LOAD=" + resp.code.to_s

#----- REST-client --- end
end

#-------------------------------------------------------------------------------------------------------------
def historical_update_instance
#TODO check presence of the instance in the historical repo. Correct pre-condition: such instance exists, else -  ERROR !
 
  
  
end

#-------------------------------------------------------------------------------------------------------------
def histStateEnd
# USE SESAME-Specific HTTP REST Interface
 addr = Infoport::Application.config.historical_repo
 pred = "<#{Infoport::Application.config.rdf_baseuri}TimeEnd>"
 state_type = "<#{Infoport::Application.config.rdf_baseuri}State>"
 timestamp = DateTime.now
 query_text = "INSERT {?s #{pred} \"#{timestamp}\"^^xsd:dateTime } WHERE {?s <#{RDF::type.to_s}>  #{state_type}."
 query_text << " ?s <#{RDF::subject.to_s}> <#{@uri}>. FILTER (! EXISTS {?s #{pred} ?time }) }"

 # puts "HISTORY DLETE STATEMENT===>\n"+query_text

resp = RestClient.post addr +"/statements",
     {   
              :update       => query_text,    
              :content_type => "text/html;charset=utf-8"                #"application/x-www-form-urlencoded"        
     }
    
    resp = resp.code   
    # puts resp  
  
  


  
  
  
end


#=========================================== ACCESSORS OF HISTORICAL REPOSITORY ===============================

def self.getPredicateTimeLine (subject_url, predicate, datetime_start, datetime_end )
     addr = Infoport::Application.config.historical_repo
     client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
     
     state_type = "<#{Infoport::Application.config.rdf_baseuri}State>"
     ts_pred    = "<#{Infoport::Application.config.rdf_baseuri}TimeStart>"
     te_pred    = "<#{Infoport::Application.config.rdf_baseuri}TimeEnd>"
     
     
     query_text = "SELECT ?l ?dts ?dte WHERE {?s <#{RDF::type.to_s}> #{state_type}. ?s <#{RDF::subject.to_s}> <#{subject_url}>. "
     query_text << "?s <#{RDF::predicate.to_s}> <#{predicate}>. ?s <#{RDF::object.to_s}> ?l. " 
     query_text << "?s #{ts_pred} ?dts. FILTER (?dts < \"#{datetime_end}\"^^xsd:dateTime) "
     query_text << " OPTIONAL {?s #{te_pred} ?dte. FILTER (?dte > \"#{datetime_start}\"^^xsd:dateTime) } "
     query_text << " } ORDER BY (?l) (?dts)"
     
     
     #puts "QUERY ----->"+query_text
     solutions = client.query(query_text);
     
     results = []
     
     if solutions.present?
         solutions.each do |s|
          results << {:value => s[:l].to_s, :start => s[:dts].to_s , :end => s[:dte].to_s }
         end
     end
     
    results 
end


end

