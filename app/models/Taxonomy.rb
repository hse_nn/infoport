require 'singleton'

class Taxonomy
  include Singleton
  
  def initialize 
    puts "I'm being initialized!"
    @taxonomy_hash = Hash.new
   
      addr = Infoport::Application.config.taxonomy_repo
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
      
      #query_text= "SELECT  ?l  WHERE {<http://www.grnti.ru/core#01_00_00> <http://www.w3.org/2004/02/skos/core#narrower> ?c . ?c <http://www.w3.org/2004/02/skos/core#prefLabel> ?l }"   

      ['01','02', '03', '04', '05', '06', '10', '11', '12','13','14','15','16','17','18','19','20','21','26','27','28','29','30','31','34','36','37','38','39','41','43','44','45','47','49','50','52','53','55','58','59','60','61','62','64','65','66','67','68','69','70','71','72','73','75','76','77','78','80','81','82','83','84','85','86','87','89','90'].each do |k| 
        
      solutions = client.query("SELECT ?l  WHERE {<http://www.grnti.ru/core#"+k+"_00_00> <http://www.w3.org/2004/02/skos/core#prefLabel> ?l }");
#
#      puts "solutions--->"
      if solutions.present?
        @taxonomy_hash[solutions.first[:l]] = "<http://www.grnti.ru/core##{k}_00_00>"
      
       solutions = client.query("SELECT ?c ?l  WHERE {<http://www.grnti.ru/core#"+k+"_00_00> <http://www.w3.org/2004/02/skos/core#narrower> ?c. ?c <http://www.w3.org/2004/02/skos/core#prefLabel> ?l }");

         solutions.each do |s|
             @taxonomy_hash["&nbsp;&nbsp;#{s[:l]}".html_safe] = "<"+s[:c]+">"
         end
      end
    end
    
   rescue Exception # broader than SPARQL::Client::ServerError 
   @taxonomy_hash = nil
 end
   
  
  def getTaxonomy
# TODO make smarter solution for reuse
    if @taxonomy_hash.blank?
      initialize
    end
    @taxonomy_hash
  end    
end
  
#TODO implement more elegant solution    
#@@e_library_taxonomy = nil
#{
#   "Естественные науки" => "URI_1",
#    "-- математика"  =>  "URI_11",
#    "--   физика" => "URI_12",
#    "Технические науки" => "URI_2",
#    "--   информатика" => "URI_21",
#    "--   электроника" => "URI_22",
#    "Социально-экономические науки" => "URI_3",
#    "--   экономика" => "URI_31",
#    "--   социология" => "URI_32",
#    "--   менеджмент" => "URI_33",
#    "--   лингвистика" => "URI_34" }
 
