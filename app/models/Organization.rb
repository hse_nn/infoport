require 'singleton'

class Organization
  include Singleton


 def initialize 
        @org_hash = Hash.new
      
      addr = Infoport::Application.config.org_repo
      client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"})
      
            
        query_text = "SELECT ?n  WHERE {<http://www.hse.ru/ontologies/v1/infoport#NNCampus> <http://xmlns.com/foaf/0.1/name> ?n }"
        solutions = client.query(query_text);
        @org_hash[solutions.first[:n]] = "http://www.hse.ru/ontologies/v1/infoport#NNCampus" 
        query_text = "SELECT ?f ?nf ?d ?nd WHERE {<http://www.hse.ru/ontologies/v1/infoport#NNCampus> <http://www.hse.ru/ontologies/v1/infoport#hasFaculty> ?f.  ?f <http://xmlns.com/foaf/0.1/name> ?nf."
        query_text << " OPTIONAL { ?f <http://www.hse.ru/ontologies/v1/infoport#hasDepartment> ?d. ?d <http://xmlns.com/foaf/0.1/name> ?nd. } } order by (?f) "
                
        solutions = client.query(query_text);
             name = nil
             solutions.each do |s|            
               
                 if name != s[:nf]
                   name = s[:nf]
                   @org_hash["&nbsp;&nbsp;#{name}".html_safe] = s[:f]
                 end
                  if s[:nd].present?
                   @org_hash["&nbsp;&nbsp;&nbsp;&nbsp;#{s[:nd]}".html_safe] = s[:d]
                  end 
             end
      
   rescue Exception  
   @org_hash = nil        
 end

def getOrganization
  @org_hash
end

end
  

# DONE  implement more elegant solution
# this structure is intialized from the repository
#@@org_structure = nil
#{
#  "Весь филиал" => "http://www.hse.ru/ontologies/v1/infoport#NNCampus",
#  "Факультет Экономики" => "URI_1",
#  "--  кафедра банковского дела" => "URI_11",
#  "-- кафедра экономической теории" => "URI_12",
#  "Факультет Менеджмента" => "URI_2",
#  "-- кафедра логистики" => "URI_21",
#  "-- кафедра венчурного менеджмента" => "URI_22",
#  "Факультет Бизнес-информатики" => "URI_3",
#  "-- кафедра математики" => "URI_31",
#  "-- кафедра прикладной математики" => "URI_32",
#  "-- кафедра информационных систем и техонологий" => "URI_33",
#  "-- базовая кафедра ТЕКОМ" => "URI_34",
#  "-- базовая кафедра МЕРА" => "URI_35",
#   "Факультет права" => "URI_4",
#   "-- кафедра гражданского права" => "URI_41",
#   "-- кафедра уголовного права" => "URI_42",
#   "Факультет гуманитарных наук" => "URI_5",
#  "-- департамент лингвистики" => "URI_51",
#  "-- департамент политологии" => "URI_52"} 
