# coding: utf-8
class FrontendController < ApplicationController
#TODO merge select2 functionality in keywords and found persons
#TODO time-based search

  
  before_filter :restore_from_session, :except => [:index]   
     
#-------------------------------------------------------------------------
def entities
   res = Hash.new
  
  res["total"]= 2 #found.size
  res["page"]=nil
  res["rows"]=[{"id"=>1, :cell=>["Doe","Bob","1950-01-10"] },{"id" =>2,:cell=>["Smith","John","1970-12-26"] } ]
  # res["keys"]=  [{"id"=>"a", "text"=>"zzzzz"}, {"id"=>"b", "text"=>"rrrrr"}]
#  res["more"]=false
 # {"rows":[
  #  {"id":1,"cell":["Doe","Bob","1950-01-10"] },
 #   {"id":2,"cell":["Smith","John","1970-12-26"] }
 #   ],
 #"page":null,
 #"total":2}
 
  
   respond_to do |format|
            format.html {render :template => "frontend/index"} # index.html.erb
            format.json { render json: res }
          end
end


#-------------------------------------------------------------------------
def keys
  
  
  found = Person.getAllKeys(params[:key_pattern])
  res = Hash.new
  
  res["total"]=found.size
  res["keys"]= found #  [{"id"=>"a", "text"=>"zzzzz"}, {"id"=>"b", "text"=>"rrrrr"}]
  res["more"]=false
  
   respond_to do |format|
            format.html {render :template => "frontend/index"} # index.html.erb
            format.json { render json: res }
          end
end
#-------------------------------------------------------------------------
  def index
    @keys = Array.new
    @e_libkeys = Array.new
    @Persons = []
    @EntityCount = 0
    @CurrentPage = 1
    
    
    session[:keys] = @keys
    session[:e_libkeys] = @e_libkeys
    session[:org_level] = nil
    session[:persons] = @Persons 
    session[:entity_count] = @EntityCount 
    session[:current_page] = @CurrentPage
      
    @anchor_dest = nil
    

    if Taxonomy.instance.getTaxonomy == nil
      #flash[:error] ='Ошибка подключения'
    end
        
  end


#-------------------------------------------------------------------------
# Actions Selector proc
#TODO parsing of actions as in post: http://stackoverflow.com/questions/8259673/how-to-trigger-different-actions-based-on-submit-button-in-rails
  def search
    @anchor_dest = 'search_results' #default destination anchor
    @flash_alert_msg =""
    @flash_notice_msg =""
        
    
    
    @org_level = params[:org_level]
    session[:org_level] = @org_level 
      
     #---- do action: search --------------  
      if params[:commit] == "search" 
        @Persons = [] 
        @anchor_dest = 'search_conds'
        @CurrentPage = 1
        session[:current_page] = @CurrentPage
        
        perform_new_search
        session[:persons] = @Persons
        @EntityCount = @Summary["entity_count"]
        session[:entity_count] = @EntityCount
        #TODO refactor code to left only one plac where we store persons in session
      end 
         
     #---- do action: delete a key from search conditions --------------     
      if params[:commit] == "keysdell" 
        @keys = @keys - (params[:keys])
        session[:keys] = @keys
        @anchor_dest = 'search_conds'
      end 
     
     #---- do action: add a key from search conditions --------------       
      if params[:commit] == "keyadd" 
        if ! @keys.include?(params[:newkey]) 
          @keys = @keys << params[:newkey]
          session[:keys] = @keys
          @anchor_dest = 'search_conds'
        end
      end 
     
     #---- do action: delete an elib taxonomy item from search conditions --------------        
      if params[:commit] == "e_lib_keysdel" 
        @e_libkeys = @e_libkeys - (params[:e_libkeys])
        session[:e_libkeys] = @e_libkeys
        @anchor_dest = 'search_conds'
      end 
     
     #---- do action: add an elib taxonomy item from search conditions --------------        
       if params[:commit] == "e_lib_keyadd" 
        if ! @e_libkeys.include?(params[:new_e_libkey]) 
          @e_libkeys = @e_libkeys << params[:new_e_libkey]
          session[:e_libkeys] = @e_libkeys
        end
        @anchor_dest = 'search_conds'
      end 
        
     #---- do action: load a user-selected set of search conditions --------------       
        if params[:commit] == "load" 
            #check presence of saved config with such name           
            cond_db = Condition.find_by_name(params[:selected_cond])
           
            if cond_db.nil?
              @flash_alert_msg = ' ОШИБКА: нет сохраненной конфигурации с таким именем'
            else
            #correct condition from db has been read. restore all params from JSON        
                condition = ActiveSupport::JSON.decode(cond_db.jsonconfig)
                @keys = condition["keys"]
                @e_libkeys = condition["elib"]
                @org_level = condition["orglevel"]
                
                #init search results
                @Persons = nil
                @Summary = nil
                @EntityCount = 0
                @CurrentPage = 0
                
                #save the same in the session object
                session[:keys] = @keys
                session[:e_libkeys] = @e_libkeys
                session[:org_level] = @org_level
                session[:persons] = @Persons 
                session[:entity_count] = @EntityCount 
                session[:current_page] = @CurrentPage 
                
                @flash_notice_msg = " конфигурация загружена"           
           end
        end    
        
        
       #---- do action: save a user-selected set of search conditions --------------        
           if params[:commit] == "save"            
            #check presence of submitted config name
            cond = Condition.find_by_name(params[:configname]) 
            if cond.nil?               
                #prepare hash object to save
                condition = Hash.new
                condition["keys"]=@keys
                condition["elib"]=@e_libkeys
                condition["orglevel"]=@org_level
                        
                #save to db with the name provided by the user in json format         
                cond = Condition.new
                cond.name = params[:configname]
                cond.jsonconfig = ActiveSupport::JSON.encode(condition) 
                cond.save!
              @flash_notice_msg = " конфигурация сохранена"  
            else
             @flash_alert_msg = ' ОШИБКА: существует сохраненная конфигурация с таким именем'
            end                    
          end 
          
        #---- do action: save a user-selected set of search conditions --------------        
           if params[:commit] == "delconf"            
            #check presence of submitted config name
            cond = Condition.find_by_name(params[:selected_cond]) 
            if cond.nil? 
              @flash_alert_msg = ' ОШИБКА: нет сохраненной конфигурации с таким именем'
            else
            cond.destroy
            @flash_notice_msg = " конфигурация удалена"              
            end                    
          end  
          
          
     flash[:alert] =  @flash_alert_msg 
     flash[:notice] = @flash_notice_msg 

     respond_to do |format|
           format.html {render :template => "frontend/index"} # index.html.erb
            #format.json { render json: "" }
     end
  end

#-------------------------------------------------------------------------
  def sel_details
  #TODO  calculate data for tag cloud, as in the following articles:
  # 1) http://www.idolhands.com/ruby-on-rails/guides-tips-and-tutorials/creating-a-heatmap-or-tag-cloud-in-rails
  # 2) http://blog.endpoint.com/2011/03/rails-tag-cloud-tutorial-spree.html
  # 3) http://mattmcnierney.wordpress.com/2011/07/30/tag-clouds-in-ruby-on-rails/
  # 4) http://railscasts.com/episodes/382-tagging?view=asciicast
  
  # 5) https://github.com/archit/jqcloud-rails !!!  
    @anchor_dest = 'entity_details'
    
    if params[:commit] == "details" && params[:found_entities].present?
          #FIXME find actual reason of exceptions there and rewrite code of the Person model (may be avoid using SPARQL client)
     begin
     @Person = Person.find_by_uri(params[:found_entities])
     rescue Exception
       begin 
         @Person = Person.find_by_uri(params[:found_entities])
       rescue
         @Person = nil
       end
     end
      
    else 
         @Summary = Person.getSummary(@org_level, @keys, @e_libkeys)
         @Person = nil

        if params[:commit] == "nextpage"
          # prepare a part of data to display the next page of found entities
          @CurrentPage = @CurrentPage + 1
          session[:current_page] = @CurrentPage
          @Persons = Person.find_by_conditions(@CurrentPage, @org_level, @keys, @e_libkeys)
          session[:persons] = @Persons
          #TODO refactor code to left only one plac where we store persons in session
        else
          if params[:commit] == "prevpage"
          # prepare a part of data to display the previous page of found entities
            @CurrentPage = @CurrentPage - 1
            session[:current_page] = @CurrentPage              
            @Persons = Person.find_by_conditions(@CurrentPage, @org_level, @keys, @e_libkeys)
            session[:persons] = @Persons
            #TODO refactor code to left only one plac where we store persons in session
            
          end
        end  
    end 
    
    
    
     
           respond_to do |format|
            format.html {render :template => "frontend/index" } # index.html.erb
            #format.json { render json: "" }
          end  
  end
#-------------------------------------------------------------------------
  def show_timeline
    @anchor_dest = 'entity_details'
  
    if params[:personuri].present?
     
     #FIXME find actual reason of exceptions there and rewrite code of the Person model (may be avoid using SPARQL client)
     begin
     @Person = Person.find_by_uri(params[:personuri])
     rescue Exception
       begin 
         @Person = Person.find_by_uri(params[:personuri])
       rescue
         @Person = nil
       end
     end
     if params[:start_date].present?
      date = Date.parse(params[:start_date])
      @StartDateSelector = date
      datetime_start = date.to_datetime
      else
       datetime_start = DateTime.now 
      end
        
     if params[:end_date].present?
      date = Date.parse(params[:end_date])
      @EndDateSelector = date
      datetime_end = date.to_datetime 
       else
        datetime_end = (DateTime.now  + 1)   
       end
      keywords_tl = Person.getPredicateTimeLine(params[:personuri], "http://www.hse.ru/ontologies/v1/infoport#FreeKeyword", datetime_start, datetime_end)
       
      if keywords_tl.present?
              #---- convert absolute DateTime values to the integers from the interval  --- begin
       int_start = datetime_start.to_i
       int_end   = datetime_end.to_i
       len = int_end - int_start
       len = 1 if len <=0 
       
       @K_int_TimeLine = []
       
       keywords_tl.each do |k| 
         ts = DateTime.iso8601( k[:start] ).to_i
          
         if ! k[:end].present?
           te = int_end
         else
           te = DateTime.iso8601(k[:end]).to_i
         end
         
         ts = int_start if ts < int_start
         te = int_end   if te > int_end
         #puts "========>"
         #puts "TS="+ts.to_s+"  , TE="+te.to_s
         
         
        @K_int_TimeLine << {:value => k[:value], :start => ( (ts - int_start) * 100 / len ) , :end => ( (te - int_start) * 100 / len )    }  
       end
      #---- convert absolute DateTime values to the integers from the interval  --- end         
      end
    end
           respond_to do |format|
            format.html {render :template => "frontend/index" } # index.html.erb
            #format.json { render json: "" }
          end  
  end

#-----------------------------------------------------------------------------------
# process selection of freekeyword from the personal keywords cloud
def get_linked_by_keys
  
  #substitute search conditions
  k  = params[:q]
  u = params[:u]
  
  # FIXME possible issues with encoding -decoding (raw text is trasmitted now from the web-browser in GET )
  #k= "математика"
  #k = k.tr('-_','+/').unpack('m')[0]
  #k = Base64.decode64(k)
   
  @keys = Array.new 
  @keys = @keys <<  k
  session[:keys] = @keys
  
  #prepare request params
    params[:org_level] = @org_level
    params[:commit] = "search" 
  
  #redirect to search action
  # redirect_to edit_multiple_items_path, :notice => 'items updated', {:page => 1, :method => 'GET'}
  #  FIXME reuse and exploit DRY  by using redirect_to like below: 
  #  redirect_to :action => search, :commit => "search", :org_level => @org_level
   
  # perform search 
    @Persons = [] 
        @anchor_dest = 'search_conds'
        @CurrentPage = 1
        session[:current_page] = @CurrentPage
        
        perform_new_search
       session[:persons] = @Persons
        @EntityCount = @Summary["entity_count"]
        session[:entity_count] = @EntityCount
        
    respond_to do |format|
           format.html {render :template => "frontend/index"} # index.html.erb
           #format.html {render :text => "RESULT IS"+ k}  
           #format.json { render json: "" }
     end
      
end



#======================== Private Methods ===================================
private 

# befor_filter action 
  def restore_from_session
      @res = params
       @keys = session[:keys]
       @e_libkeys = session[:e_libkeys]
       
       @org_level = session[:org_level]   
       @Persons = session[:persons]
       @EntityCount = session[:entity_count]
       @CurrentPage = session[:current_page]
  end


#-----------------------------------------------------------------------------
  def perform_new_search
  @Persons = Person.find_by_conditions(@CurrentPage, @org_level, @keys, @e_libkeys)
  @Summary = Person.getSummary(@org_level, @keys, @e_libkeys) 
  end


end
