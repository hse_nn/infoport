# coding: utf-8

#Dynamic Interface Controller using AJAX
class DyninterfaceController < ApplicationController
before_filter :load_conditions 

#-------------------------------------------------------------------------
def showsave
  respond_to do |format|
      format.js 
    end 
end


#-------------------------------------------------------------------------
def showload
  respond_to do |format|
      format.js  
    end 
end

#================================= PRIVATE METHODS =======================
private

 # befor_filter action 
def load_conditions
         @Conditions = Condition.all        
end  







end