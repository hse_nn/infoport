#encoding: utf-8

require 'hash_validator'

class EntitiesController < ApplicationController
  # GET /entities
  # GET /entities.json
  def index
    @persons = Person.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @persons}
    end
  end


#---------------------------------------------------------------------------------------------------------
# read an instance of Person by id
  # GET /entities/1
  # GET /entities/1.json
  def show

    data = (params[:id])
    
    # compose complete uri of the entity
    complete_uri = Infoport::Application.config.rdf_baseuri + data
    
    @person = Person.find_by_uri(complete_uri)
    
    if @person.present?
    #puts "PERSON DATA --->\n"
    #puts @person.keywords
    #puts @person.units
    #puts @person.keywords
    #puts @person.taxo_topics
    
    
    json_data = ActiveSupport::JSON.encode(@person)
    
    
        respond_to do |format|
          format.html { render text: json_data }# show.html.erb
          format.json { render text: json_data }
        end
        
    else
        respond_to do |format|
          format.html { render head: 404 }# show.html.erb
          format.json { render head: 404 }
        end  
    end
end

#---------------------------------------------------------------------------------------------------------
  # GET /entities/new
  # GET /entities/new.json
  def new
    @entity = Entity.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @entity }
    end
  end

  # GET /entities/1/edit
  def edit
    @entity = Entity.find(params[:id])
  end



#---------------------------------------------------------------------------------------------------------
# creates new instance of Person 
  # POST /entities
  # POST /entities.json
  def create
    
    data = (params[:data])
    
    
# ---- Perform data validation    --- begin  
#FIXME use filter methods to test received data !
    data_valid = false   
    if data.is_a?(Hash)
        #puts "===================== RECEIVED DATA \n======>"
        #puts data
        #puts "===================== RECEIVED DATA \n======>"
        input_validation = Person.validateData(data, Person::VALIDATION_CREATE ) # Validate using hash_validator
        data_valid = input_validation.valid?
        validation_error_msg = input_validation.errors if ! data_valid
    else
      validation_error_msg = "not a hash"  
    end    
# ---- Perform data validation    --- end
              
   if data_valid
      # compose complete uri of the entity
      complete_uri = Infoport::Application.config.rdf_baseuri + data[:id].to_s

      # check if exists such a person
      if ! Person.exist?(complete_uri)

          @person = Person.new(complete_uri, data[:lastname], data[:firstname], data[:middlename], data[:image])
          @person.keywords      = data[:keywords]
          @person.units         = data[:units]
          @person.taxo_topics   = data[:topics]
          
         # store in the transient repository
         @person.save
         # start temporal State of the instance in the historic repository
         @person.histStateStart 
          
         respond_to do |format|
            format.html { render text: "OK", status: :created }
            format.json { render text: "OK", status: :created}
         end          
      else
      # error - such person exists  
          respond_to do |format|
            format.html { render json: "FOUND", status: :found } # FIXME correct location: person }
            format.json { render json: "FOUND", status: :found } # FIXME correct location: person}
          end     
      end
    else
    # error - wrong format of input data   
          respond_to do |format|
            format.html { render json: validation_error_msg, status: 412 } #:precondition_failed }
            format.json { render json: validation_error_msg, status: 412 } #:precondition_failed }
          end     
    end

end
#---------------------------------------------------------------------------------------------------------
# update an existing instance of Person by delete/create operations

  # PUT /entities/1
  # PUT /entities/1.json
  def update
   
  data = (params[:data])
 # ---- Perform data validation    --- begin  
#FIXME use filter methods to test received data !
    data_valid = false   
    if data.is_a?(Hash)
        #puts "===================== RECEIVED DATA \n======>"
        #puts data
        #puts "===================== RECEIVED DATA \n======>"
        input_validation = Person.validateData(data, Person::VALIDATION_CREATE ) # Validate using hash_validator
        data_valid = input_validation.valid?
        validation_error_msg = input_validation.errors if ! data_valid
    else
      validation_error_msg = "not a hash"  
    end    
# ---- Perform data validation    --- end

     if data_valid
        # compose complete uri of the entity
            complete_uri = Infoport::Application.config.rdf_baseuri + params[:id]
            
            @person = Person.find_by_uri(complete_uri, false)
            
            if @person.present?
              
              #---- update history repository
              #FIXME make a smarter solution of history UPDATE 
              # finish temporal State of the instance in the historic repository
              @person.histStateEnd
              
               
              @person.destroy
              
              @person = Person.new(complete_uri, data[:lastname], data[:firstname], data[:middlename], data[:image])
              @person.keywords      = data[:keywords]
              @person.units         = data[:units]
              @person.taxo_topics   = data[:topics]
              
              @person.save
              
              #---- update history repository
              #FIXME make a smarter solution of history UPDATE 
              # start temporal State of the instance in the historic repository
              @person.histStateStart 
          
             respond_to do |format|
                format.html { render text: "OK", status: :ok }
                format.json { render text: "OK", status: :ok }
             end   
            else
              respond_to do |format|
                  format.html { head :not_found }
                  format.json { head :not_found }
              end
            end  
     else
        # error - wrong format of input data   
          respond_to do |format|
            format.html { render json: validation_error_msg, status: 412 } # Precondition failed
            format.json { render json: validation_error_msg, status: 412 } # Precondition failed
          end       
     end   
   end

#---------------------------------------------------------------------------------------------------------
# delete an instance of Person by id

  # DELETE /entities/1
  # DELETE /entities/1.json
 def destroy

      data = (params[:id])
# compose complete uri of the entity
      complete_uri = Infoport::Application.config.rdf_baseuri + data

      @person = Person.find_by_uri(complete_uri, false)

      if @person.present?
         # finish temporal State of the instance in the historic repository
        @person.histStateEnd 
 
        # destroy person instance in the transient repository
        @person.destroy
            
        respond = :no_content
      else
        respond = :not_found
      end
  
      respond_to do |format|
          format.html { head respond }
          format.json { head respond }
      end
 end


#================================================ PRIVATE METHODS =========================

# TODO seek a way to catch JSON parsing exceptions inside the controller without any monkey patches !     
end
