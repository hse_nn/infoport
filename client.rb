#encoding: utf-8

require 'rdf'
require 'sparql/client'
require 'rest-client'



  unituri = "<http://www.hse.ru/ontologies/v1/infoport#NNCampus>"
  #k = @keys.first
  
  @k1 = 'история'
  @k = @k1.encode("utf-8")

  # @k = URI::encode(@k1)

  query_text= "SELECT  *  WHERE {?s <http://www.w3.org/2004/02/skos/core#prefLabel> #{ @k } .}"   
  query_text_a= "INSERT DATA {<http://www.w3.org/test> <http://www.w3.org/2004/02/skos/core#prefLabel> \"#{ @k }\"^^xsd:string .}"
  query_text2 = query_text_a #.encode("Windows-1252")
  #query_text="INSERT { <http://example/egbook> <http://www.w3.org/2004/02/skos/core#prefLabel>  ""Это заголовок"" }"
  
  #query_text = "SELECT * WHERE {?s <http://xmlns.com/foaf/0.1/name> \"Кафедра логистики и производственного менеджмента\"^^<http://www.w3.org/2001/XMLSchema#string>   }" #FILTER regex (?l,  \"^Факультет\")}"
  #<http://xmlns.com/foaf/0.1/name> <http://www.w3.org/2004/02/skos/core#prefLabel>
  puts "------>"
  puts query_text2
  puts "====\n"
  
  
turtle_data = <<-EOTURTLE
  
@prefix : <http://www.hse.ru/ontologies/v1/infoport#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix inpo: <http://www.hse.ru/ontologies/v1/infoport#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@base <http://www.hse.ru/ontologies/v1/infoport> .

 <http://www.w3.org/test> skos:prefLabel "то заголовок" .
 
EOTURTLE
 


 puts turtle_data
 
 
delete_query = "DELETE { ?s ?p ?o } WHERE {?s ?p ?o. FILTER (?o = \"то заголовок\")}"


    
  #query_text = "CONSTRUCT {?label <http://xmlns.com/foaf/0.1/name> \"Кафедра логистики и производственного менеджмента\"^^<http://www.w3.org/2001/XMLSchema#string>} WHERE {?s ?p ?label}"
  #client = SPARQL::Client.new(Person.repository.uri.to_s, :options=>{:charset=>"UTF-8",:encoding=>::Encoding::UTF_8})
  
  
#  addr = "http://127.0.0.1:3000/test/sparqlendp"
   addr = "http://10.10.4.15/openrdf-sesame/repositories/nativerdfs"
      
  
# client = SPARQL::Client.new(addr , { :method => "get", :protocol=>"1.1"}) #, :headers => {"Content-type" => "text/html;charset=utf-8"} })
# DOES NOT WORK FOR UPDATE STATEMENTS !!! solutions = client.query(query_text2);
# DOES NOT WORK FOR DELETE solutions = client.query(delete_query);
# DOES NOT WORK FOR DELETE puts "SOLUTIONS FROM DELETE QUERY" + solutions

# USE SESAME-Specific HTTP REST Interface
# "application/x-www-form-urlencoded; charset=utf-8"
 

 headers = { :content_type => "application/x-turtle" } #OK
# resp = RestClient.post( addr  + "/statements", turtle_data, headers  ) # OK

headers = { :content_type => "application/x-www-form-urlencoded; charset=utf-8" } #OK
payload = "update=" + delete_query # because hash variant does not work ! #OK
resp = RestClient.post( addr + "/statements" , payload , headers  )    #OK
  puts resp  
  puts "CODE:" + resp.code.to_s
    
  
  



    
    
  