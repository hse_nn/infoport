# require 'rdf/ntriples'
# require 'rdf/sesame'

Infoport::Application.config.taxonomy_repo    = 'http://10.10.4.15/openrdf-sesame/repositories/infoport_taxonomy'
Infoport::Application.config.org_repo         = 'http://10.10.4.15/openrdf-sesame/repositories/infoport_org'
Infoport::Application.config.person_repo      = 'http://10.10.4.15/openrdf-sesame/repositories/nativerdfs'
Infoport::Application.config.test_repo        = 'http://10.10.4.15/openrdf-sesame/repositories/simpletest'
Infoport::Application.config.historical_repo  = 'http://10.10.4.15/openrdf-sesame/repositories/historical_store'


Infoport::Application.config.rdf_baseuri  = 'http://www.hse.ru/ontologies/v1/infoport#'
Infoport::Application.config.grnti_baseuri  = 'http://www.grnti.ru/core#'

 

