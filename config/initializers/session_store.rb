# Be sure to restart your server when you modify this file.

#disable cookie-based default store
#Infoport::Application.config.session_store :cookie_store, key: '_infoport_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
#enable CacheStore
Infoport::Application.config.session_store :cache_store
# Infoport::Application.config.session_store :active_record_store
