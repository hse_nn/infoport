#encoding:utf-8

# https://github.com/ruby-rdf/rdf-sesame

require 'rdf'
require 'sparql/client'
require 'rdf/sesame.rb'
require 'rest-client'



#REST-based METHOD http://www.openrdf.org/doc/sesame2/system/ch08.html#d0e314
#see also: https://github.com/al3xandr3/ruby/blob/master/sesame.rb

#TODO design and implement a generic solution for different RDF-servers

rest_service_addr      = 'http://10.10.4.15/openrdf-sesame/repositories/simpletest'

      
begin
  
  choice = "REST"
  literal_enc = "OK - Тест использования REST".encode("windows-1251")
  literal_enc = "OK - Тест использования REST" #.encode("utf-16")
  
  if choice == "REST"  
    #client = SPARQL::Client.new(addr ) # , {:method => 'get', :protocol=> "1.1"}) 
    #query_text = "SELECT * WHERE {?s ?o ?p.}" 
    #solutions = 
    #client.update(query_text)
    #client.query(query_text);\
    #puts solutions.inspect
    #, :headers => {"Content-type" => "text/html;charset=utf-8"} })
    
    
    puts 'insert statements using REST-based interface' 
    
    query_text = "INSERT DATA { <http://example/book3> <http://example/title>  \" #{literal_enc} \" ; <http://example/rrr>  \" #{literal_enc} \" . }" 
    
    qt = query_text.encode('windows-1251')
    puts "ENCODING OF QT=" + qt.encoding.name
    puts "ENCODING OF QUERY_TEXT =" + query_text.encoding.name
    
   # query_text = "DELETE {?s ?p ?v} WHERE {?s ?p ?v }"  
     
     
    resp = RestClient.post rest_service_addr +"/statements",
     {   
              :update       => query_text,    
              :content_type => "text/html; charset=utf-8"                #"application/x-www-form-urlencoded"        
     }
    
    resp = resp.code   
    puts resp
    
else
  
puts 'use rdf.rb interface'
url = rest_service_addr 

repository = RDF::Sesame::Repository.new(url)

#repository = RDF::Repository.new (url)

RDF::Transaction.execute(repository) do |tx|
  subject = RDF::URI("http://example.org/article")
 # tx.delete [subject, RDF::DC.title, "Old title"]
  tx.insert [subject, RDF::DC.title, literal_enc ]
end


puts "rrrr"

puts __ENCODING__.name
 

  
end      
    
    
    
    
    
rescue Exception => e   
  puts e.message   
  puts e.backtrace.inspect   
end 