class CreateConditions < ActiveRecord::Migration
  def change
    create_table :conditions do |t|
      t.string :jsonconfig
      t.string :name

      t.timestamps
    end
  end
end
